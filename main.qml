import QtQuick 2.0
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.6
import QtQuick.Controls 1.4

Grid {
    columns: 1
    spacing: 2

    //Rectangle 1
    Rectangle {
        id: rectangle1
        width: 1000
        height: 100
        color: "lightblue"
        visible: true

        Image {
            anchors.left: rectangle1.left
            id: logo
            fillMode: Image.PreserveAspectFit
            height: parent.height
            source: "logo.png"
        }

        FastBlur {
            anchors.fill: logo
            source: logo
            radius: 5
        }

        Text {
            id: helloText
            text: "Hello world!"
            textFormat: Text.AutoText
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 40; font.bold: true
        }

    }

    //Rectangle 2
    Rectangle {
        id: rectangle2
        width: 640
        height: 386

        Video {
            id: video
            width: 640
            height: 386
            source: "linux.mp4"
            autoPlay: false
            focus: true
            seekable: true
            Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()

        }

        Rectangle {
            id: progressbar
            anchors.top: playbar.bottom
            color: "yellow"
            //width: 50
            width: video.bufferProgress*playbar.width
            height: 50
        }


        Rectangle {
            id: playbar
            anchors.top: video.bottom
            width: video.width
            height: 10
            color: "blue"
        }


        Row {
            anchors.top: playbar.bottom

            Button {
                text: video.playbackState == MediaPlayer.PlayingState ? "pause" : "play"
                onClicked: {
                    video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
                }


            }


        }

    }

}
