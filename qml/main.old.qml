import QtQuick 2.0
import QtQuick.Window 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.6

Rectangle {
    id: rectangle1
   width: 1000
   height: 100
   color: "lightblue"
   visible: true

    Text {
       id: helloText
       text: "Hello world!"
       textFormat: Text.AutoText
       anchors.verticalCenter: parent.verticalCenter
       anchors.horizontalCenter: parent.horizontalCenter
       font.pointSize: 44; font.bold: true
   }

   Image {
     id: logo
     fillMode: Image.PreserveAspectFit
     height: parent.height;
     source: "logo.png"
   }

   FastBlur {
     anchors.fill: logo
     source: logo
     radius: 5
   }

   Video {
     id: video
    //  width: 640
    //  height: 386
     source: "linux.mp4"
     autoPlay: false
     focus: true
     Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
   }

   FastBlur {
     anchors.fill: video
     source: video
     radius: 5
   }


}
